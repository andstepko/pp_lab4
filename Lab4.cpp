// Lab4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <omp.h>
#include <math.h>
#include "windows.h"
#include "intrin.h"

#define MyCreateThread(func, parametr) CreateThread(0, 0, func, parametr, 0, 0);

using namespace std;

double pi;
HANDLE mutex;

struct Range
{
	int left;
	int right;
};

double vallis(int iterations)
{
	double mult = 1;
	double numerator = 0;
	for (int i = 0; i < iterations; i++)
	{
		numerator += 2;
		mult *= (numerator * numerator)
			/ ((numerator - 1) * (numerator + 1));
	}
	return mult * 2;
}

double ancient(int iterations)
{
	double sum = 0;
	for (int i = 0; i < iterations; i++)
	{
		if (i % 2 == 0)
		{
			sum += 1.0 / (i * 2 + 1);
		}
		else
		{
			sum -= 1.0 / (i * 2 + 1);
		}
	}
	return sum * 4;
}

double OMPAncient(int iterations)
{
	double sum = 0;

	#pragma omp parallel for reduction(+:sum)
	for (int i = 0; i < iterations; i++)
	{
		if (i % 2 == 0)
		{
			sum += 1.0 / (i * 2 + 1);
		}
		else
		{
			sum -= 1.0 / (i * 2 + 1);
		}
	}
	return sum * 4;
}

DWORD WINAPI threadFun(LPVOID parametr)
{
	Range range = *(Range*)parametr;

	double sum = 0.;
	for (int i = range.left; i < range.right; i++)
	{
		if (i % 2 == 0)
		{
			sum += 1.0 / (i * 2 + 1);
		}
		else
		{
			sum -= 1.0 / (i * 2 + 1);
		}
	}
	
	WaitForSingleObject(mutex, INFINITE);
	pi += sum;
	ReleaseMutex(mutex);
	
	return 1;
}

double WinAncient(int iterations, int threadNum)
{
	pi = 0;
	int stepsForThread = iterations / threadNum;
	Range** ranges = (Range**)malloc(threadNum * sizeof(Range*));
	for (int i = 0; i < threadNum; i++){
		ranges[i] = (Range*)malloc(sizeof(Range));
		Range tmp = { i * stepsForThread, (i + 1) * stepsForThread };
		ranges[i][0] = tmp;		
	}
	HANDLE* threads = (HANDLE*)malloc(threadNum * sizeof(HANDLE));

	for (int i = 0; i < threadNum; i++)
	{
		threads[i] = MyCreateThread(threadFun, (void*)(ranges[i]));
	}

	WaitForMultipleObjects(threadNum, threads, TRUE, INFINITE);

	return pi * 4;
}

long factor(int x)
{
	long result = 1;
	for (int i = 2; i <= x; i++)
	{
		result *= i;
	}
	return result;
}

double chudnovskiy(int iterations)
{
	int c1 = 13591409;
	int c2 = 545140134;
	int c3 = 640320;

	double sum = 0;
	for (int k = 0; k < iterations; k++)
	{
		double up = pow(-1.0, k & 1) * factor(6 * k) * (c1 + c2 * k);
		double down = factor(3 * k) * pow((long double)factor(k), 3) * pow(c3, 3 * k + 1.5);
		sum += up / down;
	}

	return 1 / (12 * sum);
}

int getCoresNum()
{
	int regs[4]; 
	__cpuid(regs, 4);
	int cores = (regs[0] >> 26) & 63;
	cores += 1;
	return cores;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int TESTS_NUMBER = 10;
	__int64 start = 0, finish = 0;
	__int64 diffSimple = MAXINT64;
	__int64 diffOMP = MAXINT64;
	__int64 diffWin = MAXINT64;
	__int64 frequency;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);

	double res1, res2, res3;

	//int threadNum = getCoresNum();
	int threadNum = omp_get_max_threads();

	/*int vallisN = 10000000;
	printf("vallis==>%.16f (iterations==>%i)\n", vallis(vallisN), vallisN);

	int ancientN = 10000000;
	printf("ancient==>%.16f (iterations==>%i)\n", ancient(ancientN), ancientN);

	int chudnovskiyN = 1;
	printf("chudnovskiy==>%.16f (iterations==>%i)\n", chudnovskiy(chudnovskiyN), chudnovskiyN);*/


	int ancientN = 10000000;
	// COMPARE SPEED
	for (int t = 0; t < TESTS_NUMBER; t++) {
		QueryPerformanceCounter((LARGE_INTEGER *)&start);
		res1 = ancient(ancientN);
		QueryPerformanceCounter((LARGE_INTEGER *)&finish);
		if (finish - start < diffSimple) {
			diffSimple = finish - start;
		}
	}
	printf("Simple. result==>%.16f time==>%f ms\n", res1, diffSimple * 1000.0 / frequency);
	// omp
	for (int t = 0; t < TESTS_NUMBER; t++) {
		QueryPerformanceCounter((LARGE_INTEGER *)&start);
		res2 = OMPAncient(ancientN);
		QueryPerformanceCounter((LARGE_INTEGER *)&finish);
		if (finish - start < diffOMP) {
			diffOMP = finish - start;
		}
	}
	printf("OMP. result==>%.16f time==>%f ms\n", res2, diffOMP * 1000.0 / frequency);
	// Win
	for (int t = 0; t < TESTS_NUMBER; t++) {
		QueryPerformanceCounter((LARGE_INTEGER *)&start);
		res3 = WinAncient(ancientN, threadNum);
		QueryPerformanceCounter((LARGE_INTEGER *)&finish);
		if (finish - start < diffWin) {
			diffWin = finish - start;
		}
	}
	printf("Win parallel. result==>%.16f time==>%f ms\n", res3, diffWin * 1000.0 / frequency);



	getchar();

	return 0;
}

